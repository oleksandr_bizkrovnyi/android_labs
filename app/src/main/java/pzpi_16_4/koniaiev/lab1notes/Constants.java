package pzpi_16_4.koniaiev.lab1notes;

public interface Constants {

    String KEY_NOTE = "key_note";
    String KEY_NOTES = "key_notes";

    int CODE_EDIT_NOTE = 0;
    int CODE_PICK_IMAGE = 1;
    int REQUEST_CAPTURE_IMAGE = 100;
}
