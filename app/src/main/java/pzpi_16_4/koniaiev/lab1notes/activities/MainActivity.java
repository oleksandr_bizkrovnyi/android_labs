package pzpi_16_4.koniaiev.lab1notes.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;

import pzpi_16_4.koniaiev.lab1notes.Constants;
import pzpi_16_4.koniaiev.lab1notes.R;
import pzpi_16_4.koniaiev.lab1notes.adapters.NotesAdapter;
import pzpi_16_4.koniaiev.lab1notes.data.DatabaseHelper;
import pzpi_16_4.koniaiev.lab1notes.data.Note;

public class MainActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private RecyclerView notesRV;
    private NotesAdapter notesAdapter;
    private List<Note> notes = new ArrayList<>();
    private String searchQuery;
    private boolean firstFilter;
    private boolean secondFilter;
    private boolean thirdFilter;
    private boolean isSort;
    private DatabaseHelper db;


    @SuppressWarnings("unchecked")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        toolbar = findViewById(R.id.toolbar);
        notesRV = findViewById(R.id.notes_rv);
        if (savedInstanceState != null) {
            searchQuery = (String) savedInstanceState.get("search_query");
            firstFilter = (boolean) savedInstanceState.get("first");
            secondFilter = (boolean) savedInstanceState.get("second");
            thirdFilter = (boolean) savedInstanceState.get("third");
            isSort = (boolean) savedInstanceState.get("sort");
        }

        db = new DatabaseHelper(this);
        notes = db.getAllNotes();
        setupToolbar();
        initNotesRecyclerView();
    }

    private void setupToolbar() {
        setSupportActionBar(toolbar);
    }

    private void initNotesRecyclerView() {
        notesAdapter = new NotesAdapter(notes, (itemView, position) -> itemView.setOnCreateContextMenuListener(new ContextMenuRecyclerView(position)));
        notesRV.setAdapter(notesAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_action_notes, menu);

        MenuItem myActionMenuItem = menu.findItem(R.id.action_search);
        MenuItem first = menu.findItem(R.id.action_filter_first_class);
        first.setChecked(firstFilter);
        notesAdapter.filter(Note.ClassImportance.FirstClass,!firstFilter);
        MenuItem second = menu.findItem(R.id.action_filter_second_class);
        second.setChecked(secondFilter);
        notesAdapter.filter(Note.ClassImportance.SecondClass,!secondFilter);
        MenuItem third = menu.findItem(R.id.action_filter_third_class);
        third.setChecked(thirdFilter);
        notesAdapter.filter(Note.ClassImportance.ThirdClass,!thirdFilter);
        MenuItem sort = menu.findItem(R.id.action_sort_date);
        sort.setChecked(isSort);
        notesAdapter.sort(!isSort);

        final SearchView searchView = (SearchView) myActionMenuItem.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                System.out.println("change");
                if (TextUtils.isEmpty(newText)) {
                    notesAdapter.search("");
                } else {
                    notesAdapter.search(newText);
                    searchQuery = newText;
                }
                return true;
            }
        });
        if (searchQuery != null) {
            myActionMenuItem.expandActionView();
            searchView.setQuery(searchQuery, false);
        }
        return super.onCreateOptionsMenu(menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_add:
                long id = 0;
                if (notes.size() != 0) {
                    id = notes.get(notes.size() - 1).getId() + 1;
                }
                Note note = new Note(id);
                CreateNoteActivity.openResultCreateNoteActivity(MainActivity.this, note);
                break;
            case R.id.action_filter:
                break;
            case R.id.action_sort_date:
                isSort = !item.isChecked();
                notesAdapter.sort(item.isChecked());
                break;
            case R.id.action_filter_first_class:
                firstFilter = !item.isChecked();
                notesAdapter.filter(Note.ClassImportance.FirstClass, item.isChecked());
                break;
            case R.id.action_filter_second_class:
                secondFilter = !item.isChecked();
                notesAdapter.filter(Note.ClassImportance.SecondClass, item.isChecked());
                break;
            case R.id.action_filter_third_class:
                thirdFilter = !item.isChecked();
                notesAdapter.filter(Note.ClassImportance.ThirdClass, item.isChecked());
                break;
        }
        switch (item.getItemId()) {
            case R.id.action_filter_first_class:
            case R.id.action_filter_second_class:
            case R.id.action_filter_third_class:
            case R.id.action_sort_date:
                item.setChecked(!item.isChecked());
                break;
        }
        return true;
    }

    @Override
    protected void onStart() {
        super.onStart();
        // notes = (ArrayList<Note>) SerializeUtils.deserialize("note.ser");
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case Constants.CODE_EDIT_NOTE:
                if (resultCode == RESULT_OK && data != null) {
                    Note note = (Note) data.getSerializableExtra(Constants.KEY_NOTE);
                    int index = -1;
                    for (int i = 0; i < notes.size(); ++i) {
                        if (notes.get(i).getId().equals(note.getId())) {
                            index = i;
                            break;
                        }
                    }
                    if (index == -1) {
                        notesAdapter.addNote(note);
                        db.insertNote(note);
                    } else {
                        notesAdapter.changeNote(note, index);
                        db.updateNote(note);
                    }
                }
                break;
        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        System.out.println("save instance");
        System.out.println(firstFilter);
        outState.putString("search_query", searchQuery);
        outState.putBoolean("first",firstFilter);
        outState.putBoolean("second",secondFilter);
        outState.putBoolean("third",thirdFilter);
        outState.putBoolean("sort",isSort);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
    }

    public class ContextMenuRecyclerView implements View.OnCreateContextMenuListener, MenuItem.OnMenuItemClickListener {
        private int position;

        ContextMenuRecyclerView(int position) {
            this.position = position;
        }

        @Override
        public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
            MenuInflater menuInflater = MainActivity.this.getMenuInflater();
            menuInflater.inflate(R.menu.menu_edit_note, menu);
            for (int index = 0; index < menu.size(); ++index) {
                menu.getItem(index).setOnMenuItemClickListener(this);
            }
        }

        @Override
        public boolean onMenuItemClick(MenuItem item) {
            switch (item.getItemId()) {
                case R.id.action_edit:
                    CreateNoteActivity.openResultCreateNoteActivity(MainActivity.this, notesAdapter.getItemByPosition(position));
                    break;
                case R.id.action_remove:
                    db.deleteNote(notesAdapter.getItemByPosition(position));
                    notesAdapter.removeNote(position);
                    break;
            }
            return false;
        }
    }
}
