package pzpi_16_4.koniaiev.lab1notes.adapters;

import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

import pzpi_16_4.koniaiev.lab1notes.R;
import pzpi_16_4.koniaiev.lab1notes.data.Note;

public class NotesAdapter extends RecyclerView.Adapter<NotesAdapter.VH> {

    public interface LongClickByItemListener {
        void onLongClick(View itemView, int position);
    }

    private List<Note> notes;
    private List<Note> filteredNotes;
    private List<Note> unsortedNotes;
    private LongClickByItemListener longClickByItemListener;
    private String searchQuery = "";
    private ArrayList<Note.ClassImportance> filterClasses = new ArrayList<>();

    public NotesAdapter(List<Note> notes, LongClickByItemListener longClickByItemListener) {
        this.notes = notes;
        this.filteredNotes = new ArrayList<>(notes);
        this.longClickByItemListener = longClickByItemListener;
    }

    public Note getItemByPosition(int position) {
        return filteredNotes.get(position);
    }

    public void removeNote(int position) {
        Note deleteNote = filteredNotes.remove(position);
        notes.remove(deleteNote);
        notifyItemRemoved(position);
    }

    public void addNote(Note note) {
        notes.add(note);
        if (checkNoteByFilter(note)) {
            filteredNotes.add(note);
            int position = filteredNotes.size() - 1;
            notifyItemInserted(position);
        }
    }

    public void changeNote(Note note, int position) {
        notes.set(position, note);
        int filteredPosition = getPositionNoteByIdInList(note, filteredNotes);
        filteredNotes.set(filteredPosition, note);
        notifyItemChanged(filteredPosition);
    }

    public void search(String searchQuery) {
        this.searchQuery = searchQuery.toLowerCase(Locale.getDefault());
        filterNotes();
    }
    public void sort(boolean remove){
        if(remove){
            if(unsortedNotes!=null) {
                notes = unsortedNotes;
            }
        } else{
            unsortedNotes = new ArrayList<>(notes);
            Collections.sort(notes);
        }
        filterNotes();
    }

    public void filter(Note.ClassImportance filterClass, boolean remove) {
        if (remove) {
            this.filterClasses.remove(filterClass);
        }
        else {
            this.filterClasses.add(filterClass);
        }
        filterNotes();
    }

    private void filterNotes() {
        filteredNotes.clear();
        for (Note note : notes) {
            if (checkNoteByFilter(note)) {
                filteredNotes.add(note);
            }
        }
        notifyDataSetChanged();
    }

    private boolean checkNoteByFilter(Note note) {
        boolean filterByClass = filterClassContainsInList(note.getClassImportance(), filterClasses);
        if (note.getText() == null) {
            return searchQuery.isEmpty() && filterByClass;
        }
        return (note.getText().toLowerCase().contains(searchQuery) || note.getTitle().toLowerCase().contains(searchQuery)) && filterByClass;
    }

    private boolean filterClassContainsInList(Note.ClassImportance classImportance, List<Note.ClassImportance> list) {
        if (list.isEmpty()) {
            return true;
        }
        for (Note.ClassImportance item : list) {
            if (item == classImportance) {
                return true;
            }
        }
        return false;
    }

    private int getPositionNoteByIdInList(Note note, List<Note> notes) {
        for (int i = 0; i < notes.size(); ++i) {
            if (note.getId().equals(notes.get(i).getId())) {
                return i;
            }
        }
        return -1;
    }

    @NonNull
    @Override
    public VH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_note, parent, false);
        return new VH(view);
    }

    @Override
    public void onBindViewHolder(@NonNull VH holder, int position) {
        Note note = filteredNotes.get(position);
        holder.bindData(note);
    }

    @Override
    public int getItemCount() {
        return filteredNotes.size();
    }

    class VH extends RecyclerView.ViewHolder {

        private ImageView iconIV;
        private ImageView classImportanceIV;
        private TextView timeTV;
        private TextView titleTV;
        private TextView textTV;

        VH(View itemView) {
            super(itemView);
            iconIV = itemView.findViewById(R.id.icon_iv);
            classImportanceIV = itemView.findViewById(R.id.class_importance_iv);
            timeTV = itemView.findViewById(R.id.time_tv);
            titleTV = itemView.findViewById(R.id.title_tv);
            textTV = itemView.findViewById(R.id.text_tv);

            itemView.setOnLongClickListener(v -> {
                longClickByItemListener.onLongClick(v, getAdapterPosition());
                return false;
            });
        }
        void bindData (Note note) {
            if(note.getIcon()!=null) {
                Uri uri = Uri.parse(note.getIcon());
                System.out.println(uri.toString());
                Picasso.get()
                        .load(uri)
                        .centerCrop()
                        .fit()
                        .into(iconIV);
            }
            titleTV.setText(note.getTitle());
            if (note.getText() == null) {
                textTV.setText("");
            }
            else {
                textTV.setText(note.getText());
            }
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
            String formatDateTime = note.getCreateTimeToDisplay().format(formatter);
            timeTV.setText(formatDateTime);
            switch (note.getClassImportance()) {
                case FirstClass:
                    classImportanceIV.setBackground(classImportanceIV.getContext().getDrawable(R.drawable.ic_star_border_green_24dp));
                    break;
                case SecondClass:
                    classImportanceIV.setBackground(classImportanceIV.getContext().getDrawable(R.drawable.ic_star_half_yellow_24dp));
                    break;
                case ThirdClass:
                    classImportanceIV.setBackground(classImportanceIV.getContext().getDrawable(R.drawable.ic_star_red_24dp));
                    break;
            }
        }
    }
}