package pzpi_16_4.koniaiev.lab1notes.constants;

import android.content.ContentUris;
import android.net.Uri;

public class NoteConstants {

    public static final String TABLE_NAME = "NOTES";
    public static final String NOTE_ID = "ID";
    public static final String NOTE_TITLE = "TITLE";
    public static final String NOTE_TEXT = "NOTE_TEXT";
    public static final String NOTE_CREATE_TIME = "CREATE_TIME";
    public static final String NOTE_TIME_TO_DISPLAY  = "DISPLAY_TIME";
    public static final String NOTE_IMPORTANCE = "IMPORTANCE";
    public static final String NOTE_ICON_URL = "ICON_URL";
    public static final String CONTENT_AUTHORITY = "pzpi_16_4.koniaiev.lab1notes.provider";
    public static final Uri CONTENT_AUTHORITY_URI = Uri.parse("content://" + CONTENT_AUTHORITY);
    public static final String CONTENT_TYPE = "vnd.android.cursor.dir/vnd."+CONTENT_AUTHORITY+"."+TABLE_NAME;
    public  static final Uri CONTENT_URI = Uri.withAppendedPath(CONTENT_AUTHORITY_URI, TABLE_NAME);
    public static final String CREATE_NOTE_TABLE = "CREATE TABLE NOTES (ID INTEGER Primary KEY, TITLE TEXT, NOTE_TEXT TEXT, CREATE_TIME TEXT, IMPORTANCE TEXT, ICON_URL TEXT, DISPLAY_TIME TEXT);";
    public static final String GET_ALL_NOTES = "SELECT * FROM NOTES";

    public static Uri buildNoteUri(long taskId){
        return ContentUris.withAppendedId(CONTENT_URI, taskId);
    }

    public static long getNoteId(Uri uri){
        return ContentUris.parseId(uri);
    }

}
