package pzpi_16_4.koniaiev.lab1notes.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

import pzpi_16_4.koniaiev.lab1notes.constants.NoteConstants;

public class DatabaseHelper extends SQLiteOpenHelper {

    // Database Version
    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "notes_db";


    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(NoteConstants.CREATE_NOTE_TABLE);
    }

    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS NOTES");
        onCreate(db);
    }

    public long insertNote(Note note) {
        // get writable database as we want to write data
        try(SQLiteDatabase db = this.getWritableDatabase()){
            ContentValues values = new ContentValues();
            values.put(NoteConstants.NOTE_ID, note.getId());
            values.put(NoteConstants.NOTE_TITLE, note.getTitle());
            values.put(NoteConstants.NOTE_TEXT, note.getText());
            values.put(NoteConstants.NOTE_TIME_TO_DISPLAY, note.getCreateTimeToDisplay().toString());
            values.put(NoteConstants.NOTE_CREATE_TIME, note.getTime().toInstant().atZone(ZoneId.systemDefault()).toLocalDate().toString());
            values.put(NoteConstants.NOTE_IMPORTANCE, note.getClassImportance().name());
            values.put(NoteConstants.NOTE_ICON_URL, note.getIcon());


            return db.insert(NoteConstants.TABLE_NAME, null, values);
        }
    }


    public List<Note> getAllNotes() {
        List<Note> notes = new ArrayList<>();

        try(SQLiteDatabase db = this.getWritableDatabase();
            Cursor cursor = db.rawQuery(NoteConstants.GET_ALL_NOTES, null)){
            if (cursor.moveToFirst()) {
                do {
                    Note note = new Note()
                            .setId(cursor.getLong(
                                    cursor.getColumnIndex(NoteConstants.NOTE_ID)))
                            .setTitle(cursor.getString(cursor.getColumnIndex(NoteConstants.NOTE_TITLE)))
                            .setText(cursor.getString(cursor.getColumnIndex(NoteConstants.NOTE_TEXT)))
                            .setTime(java.util.Date.from(LocalDate.parse(cursor.getString(cursor.getColumnIndex(NoteConstants.NOTE_CREATE_TIME))).atStartOfDay()
                                    .atZone(ZoneId.systemDefault()).toInstant()))
                            .setClassImportance(Note.ClassImportance.valueOf(cursor.getString(
                                    cursor.getColumnIndex(NoteConstants.NOTE_IMPORTANCE))))
                            .setIcon(cursor.getString(
                                    cursor.getColumnIndex(NoteConstants.NOTE_ICON_URL)))
                            .setCreateTimeToDisplay(LocalDateTime.parse(cursor.getString(
                                    cursor.getColumnIndex(NoteConstants.NOTE_TIME_TO_DISPLAY))));

                    notes.add(note);
                } while (cursor.moveToNext());

            }
        }
        return notes;
    }


    public int updateNote(Note note) {
        try(SQLiteDatabase db = this.getWritableDatabase()){

            ContentValues values = new ContentValues();
            values.put(NoteConstants.NOTE_ID, note.getId());
            values.put(NoteConstants.NOTE_TITLE, note.getTitle());
            values.put(NoteConstants.NOTE_TEXT, note.getText());
            values.put(NoteConstants.NOTE_TIME_TO_DISPLAY, note.getCreateTimeToDisplay().toString());
            values.put(NoteConstants.NOTE_CREATE_TIME,  note.getTime().toInstant().atZone(ZoneId.systemDefault()).toLocalDate().toString());
            values.put(NoteConstants.NOTE_IMPORTANCE, note.getClassImportance().name());
            values.put(NoteConstants.NOTE_ICON_URL, note.getIcon());

            return db.update(NoteConstants.TABLE_NAME, values, NoteConstants.NOTE_ID + " = ?",
                    new String[]{String.valueOf(note.getId())});
        }

    }

    public void deleteNote(Note note) {
       try(SQLiteDatabase db = this.getWritableDatabase()){
           db.delete(NoteConstants.TABLE_NAME, NoteConstants.NOTE_ID + " = ?",
                   new String[]{String.valueOf(note.getId())});
       }
    }
}
