package pzpi_16_4.koniaiev.lab1notes.data;

import android.support.annotation.NonNull;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

public class Note implements Serializable, Comparable<Note> {
    private Long id;
    private String title;
    private String text;
    private Date time;
    private ClassImportance classImportance;
    private String icon;
    private LocalDateTime createTimeToDisplay;

    public Note() {

    }
    public LocalDateTime getCreateTimeToDisplay() {
        return createTimeToDisplay;
    }

    public Note setCreateTimeToDisplay(LocalDateTime createTimeToDisplay) {
        this.createTimeToDisplay = createTimeToDisplay;
        return this;
    }
    public Note(Long id) {
        this.id = id;
        this.classImportance = ClassImportance.FirstClass;
        this.time = new Date();
        createTimeToDisplay = LocalDateTime.now();
    }

    public Note(Long id, String title, String text, Date time, ClassImportance classImportance, String icon) {
        this.id = id;
        this.title = title;
        this.text = text;
        this.time = time;
        this.classImportance = classImportance;
        this.icon = icon;
    }

    @Override
    public int compareTo(@NonNull Note o) {
        return o.getTime().compareTo(getTime());
    }

    public enum ClassImportance {
        FirstClass,
        SecondClass,
        ThirdClass
    }

    public Long getId() {
        return id;
    }

    public Note setId(Long id) {
        this.id = id;
        return this;
    }

    public String getTitle() {
        return title;
    }

    public Note setTitle(String title) {
        this.title = title;
        return this;
    }

    public String getText() {
        return text;
    }

    public Note setText(String text) {
        this.text = text;
        return this;
    }

    public Date getTime() {
        return time;
    }

    public Note setTime(Date time) {
        this.time = time;
        return this;
    }

    public ClassImportance getClassImportance() {
        return classImportance;
    }

    public Note setClassImportance(ClassImportance classImportance) {
        this.classImportance = classImportance;
        return this;
    }

    public String getIcon() {
        return icon;
    }

    public Note setIcon(String icon) {
        this.icon = icon;
        return this;
    }

    @Override
    public String toString() {
        return "Note{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", text='" + text + '\'' +
                ", time='" + time + '\'' +
                ", classImportance=" + classImportance +
                ", icon='" + icon + '\'' +
                '}';
    }
}
