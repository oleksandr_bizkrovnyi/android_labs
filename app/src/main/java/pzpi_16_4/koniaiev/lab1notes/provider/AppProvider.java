package pzpi_16_4.koniaiev.lab1notes.provider;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.time.LocalDate;


import pzpi_16_4.koniaiev.lab1notes.constants.NoteConstants;
import pzpi_16_4.koniaiev.lab1notes.data.DatabaseHelper;

public class AppProvider extends ContentProvider {

    private DatabaseHelper mOpenHelper;
    private static final UriMatcher sUriMatcher = buildUriMatcher();

    public static final int FRIENDS = 100;
    public static final int FRIENDS_ID = 101;

    private static UriMatcher buildUriMatcher(){
        final UriMatcher matcher = new UriMatcher(UriMatcher.NO_MATCH);
        // content://com.metanit.tasktimer.provider/FRIENDS
        matcher.addURI(NoteConstants.CONTENT_AUTHORITY, NoteConstants.TABLE_NAME, FRIENDS);
        // content://com.metanit.tasktimer.provider/FRIENDS/8
        matcher.addURI(NoteConstants.CONTENT_AUTHORITY, NoteConstants.TABLE_NAME + "/#", FRIENDS_ID);
        return matcher;
    }

    @Override
    public boolean onCreate() {
        mOpenHelper = new DatabaseHelper(getContext());
        return true;
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, @Nullable String[] projection, @Nullable String selection, @Nullable String[] selectionArgs, @Nullable String sortOrder) {
        final int match = sUriMatcher.match(uri);
        SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();
        switch(match){
            case FRIENDS:
                queryBuilder.setTables(NoteConstants.TABLE_NAME);
                queryBuilder.appendWhere( NoteConstants.NOTE_CREATE_TIME + " = \'" + LocalDate.now().toString() + "\'");
                break;
            default:
                throw new IllegalArgumentException("Unknown URI: "+ uri);
        }
        SQLiteDatabase db = mOpenHelper.getReadableDatabase();
        return queryBuilder.query(db, projection, selection, selectionArgs, null, null, sortOrder);
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {

        final int match = sUriMatcher.match(uri);
        switch(match){
            case FRIENDS:
                return NoteConstants.CONTENT_TYPE;
            default:
                throw new IllegalArgumentException("Unknown URI: "+ uri);
        }
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues values) {
        return  null;

    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String selection, @Nullable String[] selectionArgs) {
        return  0;

    }

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues values, @Nullable String selection, @Nullable String[] selectionArgs) {
        return  0;
    }
}
